# Piece+Tree

This is based upon a design I had while working on GtkTextBuffer/GtkSourceView for GNOME Builder around 2014.
This is a cleaned up version that we can use for tracking text ranges for various use cases such as spellchecking, unlimited undo, etc.

This is the combination of a B+Tree and a PieceTable.  The B+Tree comes from a
N-ary tree which has linked-leaves and linked-branches.  The piecetable comes
from using run-lengths to denote runs of text.  However, here we allow for
arbitrary data pointers instead of a pointer to an original/change combined
with an offset as that is trivial for consumers to implement by storing the
offset in the pointer with the high-bit toggled to denote original/change.

The primary use for this is to track changes to structures like GtkTextBuffer
or GtkEditable's so that external objects can know about what regions of text
could have been invalidated and/or where. You can also use it to track changes
of a buffer incrementally across an IPC such as language server protocol so
that you keep your buffers up to date with changes from the client with
relatively low overhead.

There are a few btree techniques used in here that may not be immediately obvious.
Instead of keeping sorted items within a btree node, we break the node into two sections.
One side is used for storage of actual data items and is always filled from left to right.
The other side is used as an sorted list but with integers to the previous and next item **index**.
That means that when you scan the node, you use the offsets in the sorted list.
What this means is that you can do insertions into the node without having to resort at the cost of bsearching within the node.
Given the relatively low branching factor of nodes, that has not shown to be an issue.

Also, this is a b+tree so nodes are linked to their peers.
This drastically helps in iteration forwards and reverse since you don't have to walk the tree again to find the next item in iteration.
Further more, the tree only grows from the root, so managing these during splits and merges is trivial.
