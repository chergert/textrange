#include "gtktextregionprivate.h"

int
main (int argc,
      char *argv[])
{
  GtkTextRegion *region = _gtk_text_region_new (NULL, NULL);
  for (guint i = 0; i < 1500000; i++)
    _gtk_text_region_insert (region, i, 1, GUINT_TO_POINTER (i));
  for (guint i = 0; i < 100000; i++)
    _gtk_text_region_remove (region, 100000-1-i, 1);
  _gtk_text_region_free (region);
}
