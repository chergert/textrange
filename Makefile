all: textregion timeit

WARNINGS := \
  -Wcast-align \
  -Wdeclaration-after-statement \
  -Werror=address \
  -Werror=array-bounds \
  -Werror=empty-body \
  -Werror=implicit \
  -Werror=implicit-function-declaration \
  -Werror=incompatible-pointer-types \
  -Werror=init-self \
  -Werror=int-conversion \
  -Werror=int-to-pointer-cast \
  -Werror=main \
  -Werror=misleading-indentation \
  -Werror=missing-braces \
  -Werror=missing-include-dirs \
  -Werror=nonnull \
  -Werror=overflow \
  -Werror=pointer-arith \
  -Werror=pointer-to-int-cast \
  -Werror=redundant-decls \
  -Werror=return-type \
  -Werror=sequence-point \
  -Werror=shadow \
  -Werror=strict-prototypes \
  -Werror=trigraphs \
  -Werror=undef \
  -Werror=write-strings \
  -Wformat-nonliteral \
  -Werror=format-security \
  -Werror=format=2 \
  -Wignored-qualifiers \
  -Wimplicit-function-declaration \
  -Wlogical-op \
  -Wmissing-declarations \
  -Wmissing-format-attribute \
  -Wmissing-include-dirs \
  -Wmissing-noreturn \
  -Wnested-externs \
  -Wno-cast-function-type \
  -Wno-missing-field-initializers \
  -Wno-sign-compare \
  -Wno-unused-parameter \
  -Wold-style-definition \
  -Wpointer-arith \
  -Wredundant-decls \
  -Wstrict-prototypes \
  -Wswitch-default \
  -Wswitch-enum \
  -Wundef \
  -Wuninitialized \
  -Wunused \
  -fno-strict-aliasing

textregion: gtktextregion.c gtktextregionprivate.h testtextregion.c gtktextregionbtree.h
	$(CC) -ggdb -fno-omit-frame-pointer -o $@ $(WARNINGS) $(shell pkg-config --cflags --libs glib-2.0) gtktextregion.c testtextregion.c

timeit: gtktextregion.c gtktextregionprivate.h timeit.c gtktextregionbtree.h
	$(CC) -ggdb -fno-omit-frame-pointer -o $@ $(WARNINGS) $(shell pkg-config --cflags --libs glib-2.0) -DG_DISABLE_ASSERT gtktextregion.c timeit.c

clean:
	rm -f textregion timeit
